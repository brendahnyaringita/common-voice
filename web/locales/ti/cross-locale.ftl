## Languages

contribute = ኣበርክት
get-involved-button = ተሳተፍ
get-involved-title = ኣበርክት ናብ
get-involved-text =
    ኣብ { $lang } ኣበርክቶ ኽትገብሩ ስለ ዝገበርኩም ነመስግነኩም ። ንነፍሲ ወከፍ ቋንቋ ንምጅማርን ንምዕቃብን ኣበርቲዕና ንዓዪ ኢና 
    እተን ጕጅለታት ብኢ-መይል ኣቢለን እዋናዊ ሓበሬታ ከመሓላለፋ እየን። ወፈያ ኽትገብር እንተ ደሊኻ  ብኽብረትካ ኢ-መይልካ ኣብ ታሕቲ ሃብ።
get-involved-form-title = ነቲ { $lang } ዚብሃል እዋናዊ ሓበሬታ ንምርካብ ክታም ኣእቱ ፦
get-involved-email =
    .label = ኢመይል
get-involved-opt-in = እወ ኢ-መይል ስደደለይ። ብዛዕባ ዕቤት እዚ ቛንቋ እዚ ኣብ ሓባራዊ ድምጺ ኽሕብሮ እደሊ እየ።
get-involved-submit = ኣቕርብ
get-involved-stayintouch = ኣብ ሞዚላ ኣብ ከባቢ ተክኖሎጂ ድምጺ ማሕበረሰብ ንሃንጽ ኣሎና ። ምስ ምትዕርራያት ንሓድሽ ምንጭታት ሓበሬታ ከምኡውን ነዚ ሓበሬታ እዚ ብኸመይ ከም እትጥቀመሉ ዝያዳ ኽንሰምዕ ንደሊ ኢና።
get-involved-privacy-info = ሓበሬታኻ ብጥንቃቐ ኸም እንሕዞ ንማባጻዕ ። ኣብታ <privacyLink>ናይ  ምስጢርነት ምልክታ</privacyLink> ዘሕለፋ ዝያዳ ኣንብብ።
get-involved-success-title = ን { $language } ኣበርክቶ ንምግባር ብዕዉት መገዲ ተመዝጊብካ ኢኻ ። የቐንየለይ.
get-involved-success-text = ዝያዳ ሓበሬታ እናረኸብና ብዝኸድና መጠን ክንራኸብ ኢና ።
get-involved-return-to-languages = ናብ ቓንቃታት ተመለስ
